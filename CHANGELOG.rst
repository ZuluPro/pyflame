Version 0.3.1
-------------

* Improve Django setup instructions and fix broken link in the README.
* Fix usage without ipython installed.

Version 0.3.0
-------------

* Bump minimum supported Python version to 3.7.
* Add extras for jupyter and django in setup.cfg.

Version 0.2.1
-------------

* Fix exception if an -o/--output-file argument was specified on the command line.

Version 0.2.0
-------------

* Minor tweaks to flamegraph stack entry formatting.
* Fix incorrect IPython pyflame magic message when flamegraph.pl could not be found.
* Internal reformatting and cleanups.

Version 0.1.0
-------------

* Initial release.
